package com.jmoto.api_gateway;

import com.jmoto.api_gateway.filter.PreFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableZuulProxy
public class ApiGatewayApplication {
    private static Logger log = LoggerFactory.getLogger(ApiGatewayApplication.class);

    public static void main(String[] args) {


        SpringApplication.run(ApiGatewayApplication.class, args);
    }

    @Bean
    public PreFilter preFilter() {
        return new PreFilter();
    }



}
